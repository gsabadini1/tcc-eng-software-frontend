import axios from 'axios';

const host = 'http://localhost:8080/v1'

class Service {
  list(uri) {
    return axios.get(`${host}/${uri}`)
  }

  create(uri, payload) {
    return axios.post(`${host}/${uri}`, payload)
  }

  edit(uri, id, payload) {
    return axios.patch(`${host}/${uri}/${id}`, payload)
  }

  delete(uri, id) {
    return axios.delete(`${host}/${uri}/${id}`)
  }

}

const service = new Service()
export default service
